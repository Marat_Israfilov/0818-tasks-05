/*Программа, обрабатывающая данные структуры BOOK*/
/*Структура файла books.txt:
"Book name1"
Author1
year1

"Book name2"
Author2
year2

и т.д.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 128

struct BOOK
{
	char title[N];
	char author[N];
	int year;
};

int struct_count()
{
	int count = 0;
	int i = 1;
	FILE *file;
	char buf[N];
	file = fopen("books.txt", "r");
	
	while(fgets(buf, N, file))
	{
		if((count+1)/4 != i)
		{
			count++;
		} else
			i++;
	}
	
	fclose(file);
	return count/3;
}

void get_structs(struct BOOK *books, int count)
{
	FILE *file;
	int i;
	char buf[N];
	file = fopen("books.txt", "r");
	for(i = 0; i<count; i++)
        { 
        	fgets(books[i].title, N, file);
		books[i].title[strlen(books[i].title)-1] = '\0';
        	fgets(books[i].author, N, file);
		books[i].author[strlen(books[i].author)-1] = '\0';
        	fgets(buf, N, file);
        	books[i].year = atoi(buf);
		fgets(buf, N, file); //пропускаем пустую строку
        }
	fclose(file);
}

void print_structs(struct BOOK *books, int count)
{
	int i;
	for(i = 0; i<count; i++)
        {
        	printf("%d: %s,  %s,  %d\n", i+1, books[i].title, books[i].author, books[i].year);
        }
}

int author_cmp(const void* a, const void* b)
{	
	return strcmp(((struct BOOK*)a)->author, ((struct BOOK*)b)->author);
}

int year_cmp(const void* a, const void* b)
{	
	return ((struct BOOK*)a)->year - ((struct BOOK*)b)->year;
}

int main()
{	
	struct BOOK *my_books;
	int num;
	num = struct_count();	
	my_books = (struct BOOK *) malloc(num * sizeof(struct BOOK));
	get_structs(my_books, num);
	
	printf("\nCatalog of books:\n");
	print_structs(my_books, num);
	
	qsort(my_books, num, sizeof(struct BOOK), year_cmp);
	printf("\nThe newest book:\n");
	printf("%s,  %s,  %d\n", my_books[0].title, my_books[0].author, my_books[0].year);
	
	printf("\nThe oldest book:\n");
	printf("%s,  %s,  %d\n", my_books[num-1].title, my_books[num-1].author, my_books[num-1].year);
	
	qsort(my_books, num, sizeof(struct BOOK), author_cmp);
        printf("\nSorted catalog of books:\n");
        print_structs(my_books, num);

	free(my_books);
	return 0;
}
